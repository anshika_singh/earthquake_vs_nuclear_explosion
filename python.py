# -*- coding: utf-8 -*-
"""
Created on Mon Sep 10 13:03:21 2018

@author: admin1
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import StratifiedKFold, GridSearchCV
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report, roc_auc_score
from sklearn.model_selection import train_test_split
import warnings
import sys, os

warnings.filterwarnings('ignore')
random_state = 10
np.random.seed(random_state)

dir_path = '../../pipeline_modules/'
sys.path.append(dir_path)
data73 = 'seismogram_data_73_new.csv'
df = pd.read_csv(data73)
print(df.shape)
print(df.columns)

df.head()

# processing data
df = shuffle(df, random_state = random_state)
df_train, df_test = train_test_split(df, test_size = 0.20, random_state= random_state)
mms = StandardScaler()
X_train = mms.fit_transform(df_train.drop(['id', 'target', 'moment', 'variation'], axis=1))
Y_train = df_train['target']

# plot
from sklearn.manifold import TSNE
X_embedded = TSNE(n_components=2).fit_transform(X_train)
X_embedded.shape
plt.scatter(X_embedded[:,0], X_embedded[:,1], c=Y_train)
plt.show()

# model with grid search
from sklearn import svm
from sklearn.model_selection import RandomizedSearchCV

cv = StratifiedKFold(n_splits=5, shuffle=True, random_state = random_state)
svm_clss = svm.SVC(class_weight = 'balanced', random_state = random_state) 

param_dist = {'C': np.linspace(0.1, 10, 20), 
              'gamma': np.linspace(0.1, 0.00008, 30)}

# run randomized search
n_iter_search = 20

grid_clf = RandomizedSearchCV(estimator = svm_clss, param_distributions = param_dist, n_iter = 20, cv = cv, n_jobs=-1)
grid_clf.fit(X_train, Y_train.astype(int))
print(grid_clf.best_params_)
print(grid_clf.best_score_)
print(grid_clf.best_estimator_)

# using best parameters
from sklearn import svm

svm_model = svm.SVC(C=4, cache_size=200, coef0=0.0, class_weight='balanced',
  decision_function_shape=None, degree=3, gamma=0.08, kernel='rbf',
  max_iter=-1, probability=False, random_state = random_state, shrinking=True,
  tol=0.001, verbose=True)

svm_model.fit(X_train, Y_train)

X_test  = mms.fit_transform(df_test.drop(['id', 'target', 'moment', 'variation'], axis=1))
Y_test = df_test['target']

Y_pred = svm_model.predict(X_test)

print('1. The accuracy of the model is {}\n'.format(accuracy_score(Y_test, Y_pred)))
print('2. Classification report \n {} \n'.format(classification_report(Y_test, Y_pred)))
print('3. Confusion matrix \n {} \n'.format(confusion_matrix(Y_pred, Y_test)))
print('4. Roc_Auc score \n {}'.format(roc_auc_score(Y_pred, Y_test)))
classified_df = pd.DataFrame([], columns=['stations', 'original', 'predicted'])
misclassified_df = pd.DataFrame([], columns=['stations', 'original', 'predicted'])

for i, label in enumerate(Y_test):
    result = svm_model.predict(X_test[i, :])
    
    if result[0] == label:    
        classified_df.loc[i] = [df_test.iloc[i, 0], label, result[0]]
    else:
        misclassified_df.loc[i] = [df_test.iloc[i, 0], label, result[0]]
        

print(classified_df.head())
print('\n----------------------------------------------------------------------\n')
print(misclassified_df.head())

import pickle
data = {'miss': misclassified_df, 'good':classified_df }
output = open('classification_data.pkl', 'wb')
pickle.dump(data, output)
output.close()

# Robustness of model
data_robust = 'seismogram_data_73_new_robust.csv'
df_robust = pd.read_csv(data_robust)
df_robust = shuffle(df_robust)
df_robust_test = df_robust.drop(['id', 'target', 'moment', 'variation'], axis=1)
df_robust_target = df_robust['target']

X_test_robust  = mms.fit_transform(df_robust_test)
Y_test_robust = df_robust_target

Y_pred_robust = svm_model.predict(X_test_robust)

print('1. The accuracy of the model is {}\n'.format(accuracy_score(Y_test_robust, Y_pred_robust)))
print('2. Classification report \n {} \n'.format(classification_report(Y_test_robust, Y_pred_robust)))
print('3. Confusion matrix \n {} \n'.format(confusion_matrix(Y_pred_robust, Y_test_robust)))
print('4. Roc_Auc score \n {}'.format(roc_auc_score(Y_pred_robust, Y_test_robust)))

