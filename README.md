In this project, I have developed a machine learning model to seperate earthquake signals from nuclear explosions. 
Both earthquakes and nuclear explosions generate seismic waves that can be detected thousands of kilometers away. However, it is not 
always an easy task to separate them from each other. Although seismologists have developed methods, it is sometimes very challenging. 
For example, a recent 3.4 magnitude quake from North Korea was interpreted as 'suspected explosion' but later identified as natural. 
The following python libraries are required: NumPy, Pandas, matplotlib, scikit-learn and Keras.